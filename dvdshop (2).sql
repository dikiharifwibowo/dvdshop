-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2018 at 06:20 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dvdshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `Id_Admin` char(10) NOT NULL,
  `Nama_Admin` varchar(30) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Id_Admin`, `Nama_Admin`, `Username`, `Password`) VALUES
('1', 'admin_monera', 'admin', 'admin123');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `produk` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `barangses`
--

CREATE TABLE `barangses` (
  `id` int(5) NOT NULL,
  `produk` varchar(20) NOT NULL,
  `jumlah` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_distribusi`
--

CREATE TABLE `detail_distribusi` (
  `Id_Distribusi` char(10) NOT NULL,
  `Id_Produk` char(10) NOT NULL,
  `Jumlah` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE `detail_order` (
  `Kd_Order` char(10) NOT NULL,
  `Id_Produk` char(10) NOT NULL,
  `Id_Detail` int(5) NOT NULL,
  `Harga` int(11) NOT NULL,
  `Quantity` int(5) NOT NULL,
  `Lama_Peminjaman` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`Kd_Order`, `Id_Produk`, `Id_Detail`, `Harga`, `Quantity`, `Lama_Peminjaman`) VALUES
('F0004', 'P0001', 2, 2000, 1, 2),
('F0005', 'P0001', 3, 2000, 1, 2),
('F0006', 'P0001', 4, 2000, 1, 2),
('F0007', 'P0001', 5, 2000, 1, 1),
('F0008', 'P0001', 6, 2000, 1, 1),
('F0009', 'P0001', 7, 2000, 1, 0),
('F0010', 'P0001', 8, 2000, 1, 0),
('F0011', 'P0001', 9, 2000, 1, 0),
('F0012', 'P0001', 10, 2000, 1, 1),
('F0013', 'P0001', 11, 2000, 1, 1),
('F0014', 'P0001', 12, 2000, 1, 1),
('F0015', 'P0001', 13, 2000, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `distribusi`
--

CREATE TABLE `distribusi` (
  `Id_Distribusi` char(10) NOT NULL,
  `Id_Admin` char(10) NOT NULL,
  `Tanggal` datetime NOT NULL,
  `Total` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `distribusi`
--

INSERT INTO `distribusi` (`Id_Distribusi`, `Id_Admin`, `Tanggal`, `Total`) VALUES
('H0001', '1', '2016-10-11 15:23:27', 5),
('H0002', '1', '2016-11-04 00:43:18', 9),
('H0003', '1', '2016-11-04 00:56:07', 6),
('H0004', '1', '2016-11-05 09:07:06', 8);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `Id_Kategori` char(10) NOT NULL,
  `Nama_Kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`Id_Kategori`, `Nama_Kategori`) VALUES
('K0001', 'Baju Premium'),
('K0002', 'Batik Wanita'),
('K0003', 'Baju Pria');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `Id_Keranjang` int(5) NOT NULL,
  `Id_Produk` char(10) NOT NULL,
  `Id_Session` varchar(100) NOT NULL,
  `Tanggal_Keranjang` date NOT NULL,
  `Qty` int(4) NOT NULL,
  `harga` int(16) NOT NULL,
  `Tanggal_Peminjaman` date NOT NULL,
  `Lama_Peminjaman` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `makanan`
--

CREATE TABLE `makanan` (
  `id` int(11) NOT NULL,
  `makanan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `makanan`
--

INSERT INTO `makanan` (`id`, `makanan`) VALUES
(1, 'Jagung Bakar'),
(4, 'Monday,Tuesday,Thursday');

-- --------------------------------------------------------

--
-- Table structure for table `orderan`
--

CREATE TABLE `orderan` (
  `Kd_Order` char(10) NOT NULL,
  `Id_Pelanggan` char(10) NOT NULL,
  `Jumlah` int(5) NOT NULL,
  `Tanggal` datetime NOT NULL,
  `Total_Harga` int(15) NOT NULL,
  `status` enum('Menunggu Konfirmasi','diterima','ditolak') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderan`
--

INSERT INTO `orderan` (`Kd_Order`, `Id_Pelanggan`, `Jumlah`, `Tanggal`, `Total_Harga`, `status`) VALUES
('F0001', 'Y0008', 1, '2018-03-15 14:07:24', 90000, 'Menunggu Konfirmasi'),
('F0002', 'Y0002', 1, '2018-03-16 21:11:04', 90000, 'Menunggu Konfirmasi'),
('F0004', 'Y0002', 1, '2018-04-23 19:32:59', 4000, 'diterima'),
('F0005', 'Y0002', 1, '2018-04-27 20:46:10', 4000, 'Menunggu Konfirmasi'),
('F0006', 'Y0002', 1, '2018-04-27 20:47:28', 4000, 'Menunggu Konfirmasi'),
('F0007', 'Y0002', 1, '2018-04-27 20:52:38', 2000, 'Menunggu Konfirmasi'),
('F0008', 'Y0002', 1, '2018-04-27 21:37:38', 2000, 'Menunggu Konfirmasi'),
('F0009', 'Y0002', 1, '2018-04-27 21:38:34', 0, 'Menunggu Konfirmasi'),
('F0010', 'Y0002', 1, '2018-04-27 21:39:33', 0, 'Menunggu Konfirmasi'),
('F0011', 'Y0002', 1, '2018-04-27 21:42:55', 0, 'Menunggu Konfirmasi'),
('F0012', 'Y0002', 1, '2018-04-27 21:46:58', 2000, 'Menunggu Konfirmasi'),
('F0013', 'Y0002', 1, '2018-04-27 21:53:36', 2000, 'Menunggu Konfirmasi'),
('F0014', 'Y0002', 1, '2018-04-29 11:20:14', 2000, 'Menunggu Konfirmasi'),
('F0015', 'Y0002', 1, '2018-04-29 11:31:16', 0, 'Menunggu Konfirmasi');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `Id_Pelanggan` char(10) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(10) NOT NULL,
  `Alamat` varchar(80) NOT NULL,
  `Email` varchar(25) NOT NULL,
  `Telepon` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`Id_Pelanggan`, `Username`, `Password`, `Alamat`, `Email`, `Telepon`) VALUES
('Y0002', 'user', 'user1234', 'jogja', 'user@gmail.com', 2147483647),
('Y0003', 'budi', 'budi1234', 'jogja', 'budi@gmail.com', 2147483647),
('Y0004', 'acer', 'acer1234', 'klaten', 'acer@gmail.com', 2147483647),
('Y0005', 'latif', 'latif123', 'jogja', 'latif@gmail.com', 2147483647),
('Y0006', 'farhan', 'farhan12', 'jogja', 'farhan@gmail.com', 2147483647),
('Y0007', 'd`iah', 'diah1234', 'bumiayu', 'diahkh@gmail.com', 2147483647),
('Y0008', 'dd', '1234', 'assa', 'dikiharifwibowo@gmail.com', 524),
('Y0009', 'asd', 'sadas', 'asd', 'asd@gmail.com', 123),
('Y0010', 'susu', 'susu1234', 'sleman', 'susu@gmail.com', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman`
--

CREATE TABLE `pengiriman` (
  `kode` varchar(10) NOT NULL,
  `id_pelanggan` char(10) NOT NULL,
  `jasa` enum('JNE','POS INDONESIA','TIKI','WAHANA') NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kabupaten` varchar(20) NOT NULL,
  `kodepos` varchar(10) NOT NULL,
  `tarif` int(10) NOT NULL,
  `id_session` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengiriman`
--

INSERT INTO `pengiriman` (`kode`, `id_pelanggan`, `jasa`, `provinsi`, `kabupaten`, `kodepos`, `tarif`, `id_session`) VALUES
('V0022', 'Y0002', 'POS INDONESIA', 'Sumut', 'Kulon Progo', '7777', 15000, 'pb2bedfmq6qlnl6lr2fbj1spi5'),
('V0023', 'Y0002', 'TIKI', 'Sumut', 'Bantul', '8888', 5000, 'pb2bedfmq6qlnl6lr2fbj1spi5'),
('V0024', 'Y0002', 'POS INDONESIA', 'aceh', 'Sleman', '3333', 7000, 'pb2bedfmq6qlnl6lr2fbj1spi5'),
('V0025', 'Y0002', 'JNE', 'sumbar', 'Sleman', '66666', 7000, 'pb2bedfmq6qlnl6lr2fbj1spi5'),
('V0026', 'Y0002', 'POS INDONESIA', 'sumbar', 'Kulon Progo', '7777', 15000, 'pb2bedfmq6qlnl6lr2fbj1spi5'),
('V0027', 'Y0002', 'POS INDONESIA', 'Sumut', 'Jogja', '2222', 3000, 'pb2bedfmq6qlnl6lr2fbj1spi5'),
('V0028', 'Y0002', 'JNE', 'Yogyakarta', 'Sleman', '111111', 7000, '25n93j60th39oa5415htn6j093'),
('V0029', 'Y0002', 'POS INDONESIA', 'Riau', 'Kulon Progo', '77777', 15000, '4uo9cpi6jec39gfnv7hcuadqg0'),
('V0030', 'Y0002', 'JNE', 'Yogyakarta', 'Sleman', '52271', 7000, 'nd2ckbncvgotbep8hcj1rkpek4'),
('V0031', 'Y0002', '', '-', '-', '5544', 0, 'nd2ckbncvgotbep8hcj1rkpek4');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `Id_Produk` char(10) NOT NULL,
  `Id_Kategori` char(10) NOT NULL,
  `Nama_Produk` varchar(20) NOT NULL,
  `Harga_Produk` int(11) NOT NULL,
  `Stock` int(5) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Gambar` varchar(50) NOT NULL,
  `Tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`Id_Produk`, `Id_Kategori`, `Nama_Produk`, `Harga_Produk`, `Stock`, `Deskripsi`, `Gambar`, `Tanggal`) VALUES
('P0001', 'K0001', 'dvd avengers', 2000, 211, '<p>Sasdsa</p>\r\n', '4cfb26824ee39a422b728f781fb4a9aa.jpg', '2018-04-01 23:53:53'),
('P0003', 'K0001', 'asas', 2222, 0, '<p>as</p>\r\n', '4cfb26824ee39a422b728f781fb4a9aa.jpg', '2018-04-08 14:12:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`Id_Admin`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produk` (`produk`);

--
-- Indexes for table `barangses`
--
ALTER TABLE `barangses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_distribusi`
--
ALTER TABLE `detail_distribusi`
  ADD KEY `Id_Produk` (`Id_Produk`),
  ADD KEY `Id_Distribusi` (`Id_Distribusi`),
  ADD KEY `Id_Distribusi_2` (`Id_Distribusi`),
  ADD KEY `Id_Distribusi_3` (`Id_Distribusi`);

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`Id_Detail`),
  ADD KEY `Kd_Order` (`Kd_Order`),
  ADD KEY `Id_Produk` (`Id_Produk`),
  ADD KEY `Kd_Order_2` (`Kd_Order`),
  ADD KEY `Kd_Order_3` (`Kd_Order`),
  ADD KEY `Kd_Order_4` (`Kd_Order`),
  ADD KEY `Kd_Order_5` (`Kd_Order`),
  ADD KEY `Kd_Order_6` (`Kd_Order`),
  ADD KEY `Kd_Order_7` (`Kd_Order`),
  ADD KEY `Kd_Order_8` (`Kd_Order`),
  ADD KEY `Kd_Order_9` (`Kd_Order`),
  ADD KEY `Kd_Order_10` (`Kd_Order`),
  ADD KEY `Kd_Order_11` (`Kd_Order`);

--
-- Indexes for table `distribusi`
--
ALTER TABLE `distribusi`
  ADD PRIMARY KEY (`Id_Distribusi`),
  ADD KEY `Id_Admin` (`Id_Admin`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`Id_Kategori`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`Id_Keranjang`),
  ADD KEY `Id_Keranjang` (`Id_Keranjang`),
  ADD KEY `Id_Produk` (`Id_Produk`);

--
-- Indexes for table `makanan`
--
ALTER TABLE `makanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderan`
--
ALTER TABLE `orderan`
  ADD PRIMARY KEY (`Kd_Order`),
  ADD KEY `Id_Pelanggan` (`Id_Pelanggan`),
  ADD KEY `Id_Pelanggan_2` (`Id_Pelanggan`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`Id_Pelanggan`);

--
-- Indexes for table `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `id_pelanggan` (`id_pelanggan`),
  ADD KEY `id_pelanggan_2` (`id_pelanggan`),
  ADD KEY `id_pelanggan_3` (`id_pelanggan`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`Id_Produk`),
  ADD KEY `Id_Kategori` (`Id_Kategori`),
  ADD KEY `Id_Kategori_2` (`Id_Kategori`),
  ADD KEY `Id_Produk` (`Id_Produk`),
  ADD KEY `Id_Kategori_3` (`Id_Kategori`),
  ADD KEY `Id_Kategori_4` (`Id_Kategori`),
  ADD KEY `Size` (`Deskripsi`(1));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `barangses`
--
ALTER TABLE `barangses`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `Id_Detail` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `Id_Keranjang` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `makanan`
--
ALTER TABLE `makanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `id_prod_bar` FOREIGN KEY (`produk`) REFERENCES `produk` (`Id_Produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_distribusi`
--
ALTER TABLE `detail_distribusi`
  ADD CONSTRAINT `iddist` FOREIGN KEY (`Id_Distribusi`) REFERENCES `distribusi` (`Id_Distribusi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idpdis` FOREIGN KEY (`Id_Produk`) REFERENCES `produk` (`Id_Produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD CONSTRAINT `idpdetail` FOREIGN KEY (`Id_Produk`) REFERENCES `produk` (`Id_Produk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kd_ord` FOREIGN KEY (`Kd_Order`) REFERENCES `orderan` (`Kd_Order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `distribusi`
--
ALTER TABLE `distribusi`
  ADD CONSTRAINT `ida` FOREIGN KEY (`Id_Admin`) REFERENCES `admin` (`Id_Admin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `id_prod_ker` FOREIGN KEY (`Id_Produk`) REFERENCES `produk` (`Id_Produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderan`
--
ALTER TABLE `orderan`
  ADD CONSTRAINT `idpelangganorder` FOREIGN KEY (`Id_Pelanggan`) REFERENCES `pelanggan` (`Id_Pelanggan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD CONSTRAINT `idpelpeng` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`Id_Pelanggan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `Id_Kategori` FOREIGN KEY (`Id_Kategori`) REFERENCES `kategori` (`Id_Kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
