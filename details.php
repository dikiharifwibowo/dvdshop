<?php
session_start();
?>
<?php include('header.php'); ?>
<?php include('navbar.php'); ?>
<?php
$produk = $_GET['id'];
$query = "SELECT * FROM produk WHERE Id_Produk = '$produk'";
$hasil = mysqli_query($db, $query);
$data = mysqli_fetch_assoc($hasil);

?>
<!-- Product Detail -->
	<div class="container bgwhite p-t-35 p-b-80">
		<div class="flex-w flex-sb">
			<div class="w-size13 p-t-30 respon5">
				<div class="wrap-slick3 flex-sb flex-w">
					<div class="slick3">
						<div class="item-slick3" data-thumb="images/thumb-item-03.jpg">
							<div class="wrap-pic-w">
								<img src="images/<?= $data['Gambar'] ?>" alt="IMG-PRODUCT">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="w-size14 p-t-30 respon5">
				<h4 class="product-detail-name m-text16 p-b-13">
					<?= $data['Nama_Produk'] ?>
				</h4>

				<span class="m-text17">
					<?= $data['Harga_Produk'] ?> per hari*
				</span>
				<!--  -->
				<div class="p-t-33 p-b-60">
					<div class="flex-r-m flex-w p-t-10">
						<div class="w-size16 flex-m flex-w">
							<div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
								<!-- Button -->
								<a href="input.php?input=add&id=<?php echo $data['Id_Produk']; ?>&harga=<?php echo $data['Harga_Produk']; ?>" class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">PESAN CD/DVD</a>
							</div>
						</div>
					</div>
				</div>
				<!--  -->
				<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Deskripsi Produk
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<p class="s-text8">
							<?= $data['Deskripsi'] ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php'); ?>
