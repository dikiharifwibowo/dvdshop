	<?php include('header.php'); ?>
	<?php include('navbar.php'); ?>
	
	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-60">
		<div class="container">
			<div class="row">
				<div class="col-md-12 p-b-30">
					<form class="leave-comment" action="cek_login_admin.php" method="post"  style="width: 50%;
  margin: 0 auto; text-align: center;">
						<h4 class="m-text26 p-b-36 p-t-15">
							LOGIN ADMIN
						</h4>

						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="username" placeholder="Username">
						</div>

						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="password" name="password" placeholder="password">
						</div>

						<div class="w-size25">
							<!-- Button -->
							<button class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
								LOGIN
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<?php include('footer.php'); ?>
	